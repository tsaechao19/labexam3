
#ifndef CMPE126F17_EXAM3A_NODE_H
#define CMPE126F17_EXAM3A_NODE_H



class node
{

public:
    node *next;
    std::string data;
    int value;
    char c;
    explicit node(std::string data) : data(data), next(nullptr), value(value), c(c){}

};

#endif //CMPE126F17_EXAM3A_NODE_H
